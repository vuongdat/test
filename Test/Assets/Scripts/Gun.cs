﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour 
{
	private List<GameObject> listBullet = new List<GameObject> ();
	[SerializeField]
	private GameObject bullet;

	void Start ()
	{
		for (int i = 0; i < 10; i++) {
			GameObject bu = Instantiate (bullet, Vector3.zero, Quaternion.identity) as GameObject;
			bu.SetActive (false);
			listBullet.Add (bu);
		}
	}

	public void Shot(Vector3 v)
	{
		GameObject bullet = GetBullet();
		bullet.transform.position = v;
		bullet.SetActive (true);
	}

	private GameObject GetBullet()
	{
		for (int i = 0; i < listBullet.Count; i++)
		{
			if (listBullet [i].activeSelf == false) {
				return listBullet [i];
			}
		}
		GameObject bu = Instantiate (bullet, Vector3.zero, Quaternion.identity) as GameObject;
		bu.SetActive (false);
		listBullet.Add (bu);
		return bu;
	}
}
