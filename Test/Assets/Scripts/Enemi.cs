﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemi : MonoBehaviour {

	[SerializeField]
	private float Speed = 0.1f;
	// Use this for initialization 
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		transform.position -= new Vector3 (0f, Speed * Time.deltaTime, 0f);
		if (transform.position.y < -9f) {
			Destroy(this.gameObject);
		}

		transform.Rotate(Vector3.forward *1000f * Time.deltaTime);
	}
}
