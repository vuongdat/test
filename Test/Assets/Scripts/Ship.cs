﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {

	private Vector3 offset;
	[SerializeField]
	private Gun gun;
	[SerializeField] 
	private Transform gunPos;
	private float currentFrameShot;
	[SerializeField]
	private float frameShot=1f;
	// Use this for initialization
	void Start () {
		currentFrameShot = frameShot;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetMouseButtonDown (0))
		{
			Vector3 v = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			offset = v - transform.position;
		}
		if (Input.GetMouseButton (0)) {
			Vector3 currentPos = Camera.main.ScreenToWorldPoint (Input.mousePosition)+offset;
			transform.position = new Vector3 (currentPos.x, transform.position.y, transform.position.z);
			currentFrameShot += Time.deltaTime;
			if (currentFrameShot >= frameShot) {
				currentFrameShot = 0f;
				Shot ();
			}
		}
	}

	void Shot()
	{
		gun.Shot (gunPos.position);
	}
}
