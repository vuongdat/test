﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	[SerializeField]
	private float Speed = 10f;
	// Use this for initialization 
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += new Vector3 (0f, Speed * Time.deltaTime, 0f);
		if (transform.position.y > 6f) {
			gameObject.SetActive (false);
		}
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.CompareTag ("Enemi")) 
		{
			this.gameObject.SetActive (false);
			EnemiSpawnManager.Instance.InitAtPos (other.transform.position);
			Destroy (other.gameObject);
		}
	}
}
