﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiSpawnManager : MonoBehaviour 
{

	[SerializeField]
	private GameObject[] Enemi;
	private float minX = -3f;
	private float maxX = 3f;
	private float posY=9f;


	private float currentFrameShot;
	[SerializeField]
	private float frameShot=1f;

	public static EnemiSpawnManager Instance;

	void Start ()
	{
		Instance = this;
	}
	

	void Update () 
	{
		currentFrameShot += Time.deltaTime;
		if (currentFrameShot >= frameShot) {
			currentFrameShot = 0f;
			Vector3 pos = new Vector3 (Random.Range (minX, maxX), posY, 0f);
			InitEnemi (pos);
		}
	}

	void InitEnemi(Vector3 v)
	{
		
		GameObject enemi = Instantiate (Enemi[Random.Range(0,Enemi.Length)], v, Quaternion.identity);
	}

	public void InitAtPos(Vector3 p)
	{
		for (int i = 0; i < 3; i++) {
			Vector3 v = new Vector3 (Random.Range (p.x - 1f, p.x + 1f), Random.Range (p.y + 0f, p.y + 2f), 0f);
			InitEnemi (v);
		}
	}
}
